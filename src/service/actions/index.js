import {
  INCREAMENT,
  DECREAMENT,
  ADD_TODO,
  DELETE_TODO,
  REMOVE_TODO,
  UPDATE_TODO,
} from "../Const";

export const incNumber = (num) => {
  return {
    type: INCREAMENT,
    payload: num,
  };
};

export const drcNumber = () => {
  return {
    type: DECREAMENT,
  };
};

export const addTodo = (formData) => {
  return {
    type: ADD_TODO,
    payload: {
      id: formData.id,
      name: formData.name,
      subtext: formData.subtext,
    },
  };
};

export const deleteTodo = (id) => {
  return {
    type: DELETE_TODO,
    payload: {
      id: id,
    },
  };
};

export const UpdateTodo = (udateData) => {
  return {
    type: UPDATE_TODO,
    payload: {
      uid: udateData.id,
      uname: udateData.name,
      usubtext: udateData.subtext,
    },
  };
};

export const removeTodo = () => {
  return {
    type: REMOVE_TODO,
  };
};
