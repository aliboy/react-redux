import {
  INCREAMENT,
  DECREAMENT,
  ADD_TODO,
  DELETE_TODO,
  REMOVE_TODO,
  UPDATE_TODO,
} from "../Const";

const initialState = 0;
export const changeTheNum = (state = initialState, action) => {
  switch (action.type) {
    case INCREAMENT:
      return state + action.payload;
    case DECREAMENT:
      return state - 1;
    default:
      return state;
  }
};

const getLocalData = JSON.parse(localStorage.getItem("todo"));
const intData = getLocalData ? getLocalData : [];
const initialTodo = {
  toDolist: intData,
};

export const todoReducer = (state = initialTodo, action) => {
  switch (action.type) {
    case ADD_TODO:
      let { id, name, subtext } = action.payload;
      return {
        ...state,
        toDolist: [
          ...state.toDolist,
          {
            id: id,
            name: name,
            subtext: subtext,
          },
        ],
      };
    case DELETE_TODO:
      const result = state.toDolist.filter((e) => e.id !== action.payload.id);
      return {
        ...state,
        toDolist: result,
      };
    case UPDATE_TODO:
      let { uid, uname, usubtext } = action.payload;
      const edit = state.toDolist.map((e) =>
        e.id !== uid ? e : { ...e, id: uid, name: uname, subtext: usubtext }
      );
      return {
        ...state,
        toDolist: edit,
      };
    case REMOVE_TODO:
      return {
        ...state,
        toDolist: [],
      };
    default:
      return state;
  }
};
