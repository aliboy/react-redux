import React from "react";
import { BrowserRouter } from "react-router-dom";
import Routess from "./component/Routes";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routess />
      </BrowserRouter>
    </div>
  );
}

export default App;
