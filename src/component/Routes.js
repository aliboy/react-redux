import React from "react";
import { Routes, Route } from "react-router-dom";
import Login from "./login/Login";
import ToDo from "./todo/ToDo";
import ToDoMap from "./todo/ToDoMap";

function Routess() {
  return (
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/todo" element={<ToDo />} />
      <Route path="/todomap" element={<ToDoMap />} />
    </Routes>
  );
}

export default Routess;
