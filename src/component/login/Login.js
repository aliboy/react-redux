import { Button, Paper, TextField, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import React from "react";
import { useLocation } from "react-router-dom";

function Login() {
  function useQuery() {
    // Use the URLSearchParams API to extract the query parameters
    // useLocation().search will have the query parameters eg: ?foo=bar&a=b
    return new URLSearchParams(useLocation().search);
  }
  const query = useQuery();
  const url = query.get("url");
  console.log(url);

  const initialValues = {
    email: "",
    password: "",
  };

  const validationSchema = Yup.object({
    email: Yup.string()
      .email("Invailid email format")
      .required("Required email field"),
    password: Yup.string().required("Required password field"),
  });

  const onSubmit = (values, onSubmitProps) => {
    // window.location = `http://localhost:3001/${values.email}/${values.password}`;
    window.location =
      url + "/?email=" + values.email + "&password=" + values.password;
  };

  return (
    <Box
      sx={{
        width: 500,
        maxWidth: "100%",
      }}
      mt={5}
      mx="auto"
    >
      <Paper variant="outlined">
        <Box p={3}>
          <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
          >
            {({ isSubmitting }) => {
              return (
                <Form>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="email"
                      size="small"
                      label="Email"
                      id="email"
                      name="email"
                    />
                    <ErrorMessage name="email">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Box mb={2}>
                    <Field
                      as={TextField}
                      fullWidth
                      type="password"
                      size="small"
                      label="Password"
                      id="password"
                      name="password"
                    />
                    <ErrorMessage name="password">
                      {(msg) => (
                        <Typography
                          component="span"
                          variant="body2"
                          sx={{ color: "red" }}
                        >
                          {msg}
                        </Typography>
                      )}
                    </ErrorMessage>
                  </Box>
                  <Button
                    type="submit"
                    color="primary"
                    variant="contained"
                    disabled={isSubmitting}
                  >
                    Submit
                  </Button>
                </Form>
              );
            }}
          </Formik>
        </Box>
      </Paper>
    </Box>
  );
}

export default Login;
