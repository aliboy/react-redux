import React from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import FormControl from "@mui/material/FormControl";
import FolderIcon from "@mui/icons-material/Folder";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import {
  Avatar,
  Button,
  Container,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
  Typography,
} from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import {
  addTodo,
  deleteTodo,
  removeTodo,
  UpdateTodo,
} from "../../service/actions";

export default function ToDo() {
  // const getData = JSON.parse(localStorage.getItem("todo"));
  const initData = {
    id: "",
    name: "",
    subtext: "",
  };
  // const [mode, setMode] = React.useState(false);
  const [inputData, setInputData] = React.useState(initData);
  const [updateBtn, setUpdateBtn] = React.useState(true);
  const list = useSelector((state) => state.todoReducer.toDolist);
  localStorage.setItem("todo", JSON.stringify(list));
  const dispatch = useDispatch();

  const handleFormOnchange = (e) => {
    const newFormData = { ...inputData };
    newFormData[e.currentTarget.name] = e.currentTarget.value;
    setInputData(newFormData);
  };

  const addDataToList = (getInputValue) => {
    let existUser = list.filter((user) => user.id === inputData.id);
    if (existUser.length) {
      alert("User Already exist");
    } else {
      if (getInputValue.id) {
        dispatch(addTodo(getInputValue));
        setInputData(initData);
      } else alert("Please enter id");
    }
  };

  const editToDOList = (id) => {
    const editdo = list.find((e) => e.id === id);
    setInputData({
      ...inputData,
      id: editdo.id,
      name: editdo.name,
      subtext: editdo.subtext,
    });
    setUpdateBtn(false);
  };

  const updateToDoList = (userData) => {
    dispatch(UpdateTodo(userData));
    setInputData(initData);
  };

  return (
    <Box mt={4}>
      <Container maxWidth="sm">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <FormControl sx={{ width: "100%" }}>
              <TextField
                id="id"
                label="ID"
                name="id"
                variant="outlined"
                value={inputData.id}
                onChange={handleFormOnchange}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl sx={{ width: "100%" }}>
              <TextField
                id="name"
                label="Name"
                name="name"
                variant="outlined"
                value={inputData.name}
                onChange={handleFormOnchange}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl sx={{ width: "100%" }}>
              <TextField
                id="subtext"
                label="Designation"
                name="subtext"
                variant="outlined"
                value={inputData.subtext}
                onChange={handleFormOnchange}
              />
            </FormControl>
          </Grid>
          <Grid container spacing={2} item xs={12}>
            <Grid item>
              <Button
                variant="contained"
                disableElevation
                onClick={() => addDataToList(inputData)}
              >
                Add
              </Button>
            </Grid>
            <Grid item>
              <Button
                disabled={updateBtn}
                variant="contained"
                disableElevation
                onClick={() => updateToDoList(inputData)}
              >
                Update
              </Button>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
              ToDo List with text and icon
            </Typography>
            <List dense={false}>
              {list.map((data) => (
                <ListItem
                  key={data.id}
                  secondaryAction={
                    <>
                      <IconButton
                        edge="end"
                        onClick={() => editToDOList(data.id)}
                      >
                        <EditIcon />
                      </IconButton>
                      <IconButton
                        edge="end"
                        onClick={() => dispatch(deleteTodo(data.id))}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </>
                  }
                >
                  <ListItemAvatar>
                    <Avatar>
                      <FolderIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={data.name} secondary={data.subtext} />
                </ListItem>
              ))}
            </List>
            <Button
              color="error"
              variant="contained"
              onClick={() => dispatch(removeTodo())}
            >
              Remove All
            </Button>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}
