import React from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import FormControl from "@mui/material/FormControl";
import FolderIcon from "@mui/icons-material/Folder";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import {
  Alert,
  Avatar,
  Button,
  Container,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
  Typography,
} from "@mui/material";
import { connect } from "react-redux";
import {
  addTodo,
  deleteTodo,
  removeTodo,
  UpdateTodo,
} from "../../service/actions";

function ToDoMap({
  listMap,
  removeTodoMap,
  addTodoMap,
  UpdateTodoMap,
  deleteTodoMap,
}) {
  const initData = {
    id: "",
    name: "",
    subtext: "",
  };
  const [inputData, setInputData] = React.useState(initData);
  const [updateBtn, setUpdateBtn] = React.useState(true);
  const [alertShow, setAlertShow] = React.useState(["Err1", "Err2", "Err3"]);
  localStorage.setItem("todo", JSON.stringify(listMap));

  const handleFormOnchange = (e) => {
    const newFormData = { ...inputData };
    newFormData[e.currentTarget.name] = e.currentTarget.value;
    setInputData(newFormData);
  };

  const addDataToList = (getInputValue) => {
    let existUser = listMap.filter((user) => user.id === inputData.id);
    if (existUser.length) {
      setAlertShow(alertShow[0]);
    } else {
      if (getInputValue.id) {
        addTodoMap(getInputValue);
        setInputData(initData);
      } else setAlertShow(alertShow[1]);
    }
  };

  const editToDOList = (id) => {
    const editdo = listMap.find((e) => e.id === id);
    setInputData({
      ...inputData,
      id: editdo.id,
      name: editdo.name,
      subtext: editdo.subtext,
    });
    setUpdateBtn(false);
  };

  const updateToDoList = (userData) => {
    UpdateTodoMap(userData);
    setInputData(initData);
  };

  const AlertMessage = () => {
    return (
      <>
        {alertShow === "Err1" && (
          <Alert severity="error">
            This is an error alert — user already exist0
          </Alert>
        )}
        {alertShow === "Err2" && (
          <Alert severity="error">
            This is an error alert — user already exist1
          </Alert>
        )}
        {alertShow === "Err3" && (
          <Alert severity="error">
            This is an error alert — user already exist2
          </Alert>
        )}
      </>
    );
  };

  return (
    <Box mt={4}>
      <Container maxWidth="sm">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <FormControl sx={{ width: "100%" }}>
              <TextField
                id="id"
                label="ID"
                name="id"
                variant="outlined"
                value={inputData.id}
                onChange={handleFormOnchange}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl sx={{ width: "100%" }}>
              <TextField
                id="name"
                label="Name"
                name="name"
                variant="outlined"
                value={inputData.name}
                onChange={handleFormOnchange}
              />
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl sx={{ width: "100%" }}>
              <TextField
                id="subtext"
                label="Designation"
                name="subtext"
                variant="outlined"
                value={inputData.subtext}
                onChange={handleFormOnchange}
              />
            </FormControl>
            {alertShow && <AlertMessage />}
          </Grid>
          <Grid container spacing={2} item xs={12}>
            <Grid item>
              <Button
                variant="contained"
                disableElevation
                onClick={() => addDataToList(inputData)}
              >
                Add
              </Button>
            </Grid>
            <Grid item>
              <Button
                disabled={updateBtn}
                variant="contained"
                disableElevation
                onClick={() => updateToDoList(inputData)}
              >
                Update
              </Button>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
              ToDo List with text and icon
            </Typography>
            <List dense={false}>
              {listMap.map((data) => (
                <ListItem
                  key={data.id}
                  secondaryAction={
                    <>
                      <IconButton
                        edge="end"
                        onClick={() => editToDOList(data.id)}
                      >
                        <EditIcon />
                      </IconButton>
                      <IconButton
                        edge="end"
                        onClick={() => deleteTodoMap(data.id)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </>
                  }
                >
                  <ListItemAvatar>
                    <Avatar>
                      <FolderIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={data.name} secondary={data.subtext} />
                </ListItem>
              ))}
            </List>
            <Button
              color="error"
              variant="contained"
              onClick={() => removeTodoMap()}
            >
              Remove All
            </Button>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}

const mapStateToProps = (state) => {
  return {
    listMap: state.todoReducer.toDolist,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addTodoMap: (data) => dispatch(addTodo(data)),
    UpdateTodoMap: (data) => dispatch(UpdateTodo(data)),
    deleteTodoMap: (id) => dispatch(deleteTodo(id)),
    removeTodoMap: () => dispatch(removeTodo()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoMap);
