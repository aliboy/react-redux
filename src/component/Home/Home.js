import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { incNumber, drcNumber } from "../../service/actions/index";

function Home() {
  const myState = useSelector((state) => state.changeTheNum);
  const dispatch = useDispatch();
  return (
    <div>
      <input type="button" value="-" onClick={() => dispatch(drcNumber())} />
      <input type="text" name="num" value={myState} onChange={() => true} />
      <input type="button" value="+" onClick={() => dispatch(incNumber(5))} />
    </div>
  );
}

export default Home;
